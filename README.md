Notes regarding Azure Active Directory B2C
==========================================

We try to do as much as possible for the configuration using Terraform. However,
some things are not possible yet with the current terraform provider.

Notable: it seems it is only possible to create the Azure AD B2C, but not to
further configure it; for example for integration with applications or for
adding users or for configuring federation.