# Configure Terraform
terraform {
  required_providers {
    azuread = {
      source  = "hashicorp/azuread"
      version = "~> 2.15.0"
    }
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "=2.98.0"
    }
  }
}

# Configure the Azure Active Directory Provider
provider "azuread" {
  # PeopleWare's tenant ID
  tenant_id = local.tenant_id
}

# Configure the Azure provider (terraform state)
provider "azurerm" {
  features {}

  storage_use_azuread = true
  subscription_id     = local.subscription_id
}

# Setup Azure Active Directory B2C
resource "azurerm_aadb2c_directory" "energix-b2c" {
  country_code            = "BE"
  data_residency_location = "Europe"
  display_name            = "energix-b2c-tenant"
  domain_name             = "energixppw.onmicrosoft.com"
  resource_group_name     = "energix"
  sku_name                = "PremiumP1"
}
