# subscription level constants
locals {
  location        = "West Europe"
  subscription_id = "098ad191-f2fd-4240-9a7c-e269ab8394c0"
  tenant_id       = "6bf21748-a694-4bd1-b1c4-6563b75bde06"
}

# new users to create
locals {
  user_password = "password_1!"
  user_domain   = "@peoplewarenv.onmicrosoft.com"

  # accounts for developers
  developers_to_create = [
    {
      user_principal_name = "energix_user_1${local.user_domain}",
      display_name        = "Energix #1",
      password            = local.user_password
    },
    {
      user_principal_name = "energix_user_2${local.user_domain}",
      display_name        = "Energix #2",
      password            = local.user_password
    },
    {
      user_principal_name = "energix_user_3${local.user_domain}",
      display_name        = "Energix #3",
      password            = local.user_password
    },
    {
      user_principal_name = "energix_user_4${local.user_domain}",
      display_name        = "Energix #4",
      password            = local.user_password
    },
    {
      user_principal_name = "energix_user_5${local.user_domain}",
      display_name        = "Energix #5",
      password            = local.user_password
    },
    {
      user_principal_name = "energix_user_6${local.user_domain}",
      display_name        = "Energix #6",
      password            = local.user_password
    },
    {
      user_principal_name = "energix_user_7${local.user_domain}",
      display_name        = "Energix #7",
      password            = local.user_password
    }
  ]
}
