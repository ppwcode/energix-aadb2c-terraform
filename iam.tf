# subscription referenced by its alias
data "azurerm_subscription" "energix-sub" {}

# storage account that contains all terraform state
data "azurerm_storage_account" "tfstateenergix" {
  name                = "tfstateenergix"
  resource_group_name = "tfstate"
}
